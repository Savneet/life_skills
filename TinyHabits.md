# Tiny Habits

#### 1. In this video, what was the most interesting story or idea for you?

The idea I found most interesting is how small habits can have a long-lasting effect on our behaviour. The speaker described how he went from doing one pushup to performing 50–60 pushups in a single day. The idea was that if you want to make a significant change, you have to start with small behaviours.


#### 2. How can you use B = MAP to make making new habits easier? What are M, A and P.

B stands for Behaviour, which is what we want to do, like reading before bed. M is Motivation, our desire or energy to do the behaviour. A is Ability, how easy or hard the behaviour is for us to do. And P is Prompt, which is the cue that reminds us to do the behaviour. By adjusting your motivation, ability, and prompt, we can make it easier to start and stick to new habits.


#### 3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

It's important to shine or celebrate after each successful completion of a habit because it reinforces the behaviour and increases motivation. When we celebrate even small wins, like doing one push-up or flossing one tooth, we create a positive feeling of success. 

#### 4. In this video, what was the most interesting story or idea for you?

The most interesting part for me was the example of the Ship of Theseus. Imagine a ship that's been around for a long time. As parts wear out, they get replaced, until eventually, every piece of the ship has been changed. Similarly, small things we do regularly, like habits, slowly shape who we are. This idea made me think about how our habits not only help us succeed but also change how we see ourselves.

#### 5. What is the book's perspective about Identity?

The book emphasizes that identity change is crucial for habit formation. It says changing how you see yourself is key to making habits. Instead of just focusing on what you want to achieve, it's about becoming the type of person who naturally does those habits.

#### 6. Write about the book's perspective on how to make a habit easier to do?

To make a habit easier, the book suggests making it obvious, attractive, and simple. That means making sure you notice the cue, enjoying doing the habit, and not having many obstacles to doing it.

#### 7. Write about the book's perspective on how to make a habit harder to do?

To make a habit harder, you can hide the cue, make the habit unenjoyable, and delay any rewards. This makes it less likely you'll want to do the habit.

#### 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to form the habit of reading non-fiction books.
To make the cue obvious and make the habit attractive, the steps I would take are:

1. Make the cue obvious: Put the book where I can see it easily, like on my nightstand.

2. Make the habit more attractive: Choosing a book I'm excited about and read in a comfy spot.

3. Make the habit easy: Start with reading just a little bit each day, like one page, and do it at the same time every day.

4. Make the response satisfying: After reading, I will write down what I learned or enjoyed, and treat myself with something nice, like a snack.


#### 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I would like to eliminate the habit of eating junk food. To make the cue invisible or the process unattractive, the steps I would take are:

1. Make the cue invisible: Remove junk food from my home or store it in less accessible places.

2. Make the process unattractive: Educating myself about the negative health effects of eating junk food, such as weight gain, low energy, and increased risk of chronic diseases.
 
3. Make the habit harder: Replacing junk food with healthier alternatives, like fruits, vegetables and nuts. 

4. Make the response unsatisfying: Reflecting on how eating junk food makes me feel guilty.