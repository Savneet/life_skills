# Energy Management

#### 1. What are the activities you do that make you relax - Calm quadrant?
The activities that make me relaxed and calm are listening songs and going for a walk.

#### 2. When do you find getting into the Stress quadrant?
I get into the stress quadrant when I have a certain exam or interview coming up or when I am somehow feeling anxiety about a certain situation.

#### 3. How do you understand if you are in the Excitement quadrant?
I understand that I am in the excitement quadrant when I am extremely happy and looking forward to the thing I'm excited about. 

#### 4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

1. Sleep is crucial for learning and memory formation and for consolidating new information.
2. Sleep deprivation negatively impacts the brain's ability to retain new memories.
3. Sleep disruption is linked to cognitive decline, ageing, and Alzheimer's disease.
4. Sleep loss impairs the immune system and natural killer cell activity and also increases cancer risk.
5. Lack of sleep alters gene expression and its deprivation has catastrophic health consequences.

#### 5. What are some ideas that you can implement to sleep better?

1. Maintain a regular sleep schedule, going to bed and waking up at the same time every day.
2. Keep the bedroom cool, around 18 degrees Celsius, as a cooler temperature promotes better sleep.
3. Avoid alcohol and caffeine, which can disrupt sleep quality.
4. Limit daytime naps if struggling with nighttime sleep.
5. If awake for too long in bed, get up and go to another room until feeling sleepy again.


#### 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

1. Exercise has immediate positive effects on the brain as it improves mood, focus, and reaction times.
 
2. Regular exercise changes the brain's anatomy by producing new brain cells in the hippocampus, improving long-term memory and increasing volume in the prefrontal cortex and hippocampus.
 
3. Exercise provides long-lasting mood benefits by increasing levels of mood-boosting neurotransmitters.

4. Exercise has protective effects on the brain, strengthening the hippocampus and prefrontal cortex, which are susceptible to neurodegenerative diseases and cognitive decline.
 
5. Exercise helps delay the onset of conditions like Alzheimer's and dementia.


#### 7. What are some steps you can take to exercise more?

1. Aim for 30 minutes of aerobic exercise 3-4 times a week to get the brain-boosting benefits.
   
2. Incorporate more walking into daily routine like taking the stairs
   
3. Start with moderate activities like brisk walking or cycling. 
   
4. Gradually increase the intensity and duration of workouts over time.
