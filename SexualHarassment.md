# Prevention of Sexual Harassment

### 1. What kinds of behaviour cause sexual harassment?<br />
Sexual harassment behaviour includes comments about a person's body, and sexual or gender-based jokes or remarks. It can also be physical like inappropriate touching or sexual gesturing.
### 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour? <br />
If I encounter any such incident, I would immediately report it to people who are in the capacity to address it like the manager or HR.


  