# Apache Kafka
Let's understand Apache Kafka through an example. 

We all have used the YouTube platform. There are mainly two types of YouTube users. 
1. The users uploading the video on the platform are like "producers" who generate the data.
2. The users who watch the videos on the platform are like "consumers" who receive the data.

Apache Kafka acts like a central hub that manages the flow between producers and consumers. The consumers in Kafka subscribe to the particular topic they're interested in. For example, in the context of YouTube, consumers can subscribe to the particular channel they're interested in. 


![Apache Kafka acting as a hub managing flow between consumers and producers](https://lh6.googleusercontent.com/proxy/2iXJwssGzEio_G5kxvVYMXzCo879x3v5Vf4zhj-T3U7OSz2e0R2-z6F2UGtJmhRQFNihGOt6ReavpcaBUog3KfLwpdNOXeyDP15SfUJSyMIeeNAcfldLyzlPuDSPUxIgnQ)


Apache Kafka decouples producers and consumers which means that both producers and consumers are separated from each other and they need not know details of each other. Like in YouTube, the video producer does not know the details of the consumers of the video. 

In summary, Apache Kafka acts as a central hub for managing massive amounts of data generated and consumed on the platform.

###  References

* https://www.ibm.com/topics/apache-kafka
* https://www.youtube.com/watch?v=ei6fK9StzMM
* https://medium.com/swlh/apache-kafka-what-is-and-how-it-works-e176ab31fcd5

