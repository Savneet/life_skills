# Learning Process

#### 1. What is the Feynman Technique? Explain in 1 line.

Feynman Technique is a method to understand complex concepts by explaining them to someone else.

#### 2. In this video, what was the most interesting story or idea for you?

The most interesting idea for me was the technique of going back and forth between focus mode and diffuse mode in case of getting stuck while learning new concepts. I liked the story of Thomas Edison and how he used this idea.

#### 3. What are active and diffused modes of thinking?

There are two kinds of modes of learning. Active mode is the focused mode where you are completely focused on the work you're doing. The diffused mode represents a more relaxed state of the brain which allows creative ideas to flow in.

#### 4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

The steps to take while learning a new skill are:
1. Deconstruct the skill.
2. Learn enough to self-correct. Learn just enough to practice and self-correct.
3. Remove practice barriers (distractions) like television and, the internet.
4. Practice for at least 20 hours.


#### 5. What are some of the actions you can take going forward to improve your learning process?

Some of the actions I can take to improve my learning process are:
1. To understand complex concepts, I can use the Feynman Technique of explaining the concept to someone else.
2. I will use the idea of active and diffuse modes in case of getting stuck while learning new concepts.
3. I will practice for at least 20 hours to learn a skill.
