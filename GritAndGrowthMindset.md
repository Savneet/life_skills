# Grit And Growth Mindset

#### 1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

The characteristic that most of the people who succeed in life have is Grit. Grit is passion and perseverance for very long-term goals. Grit is putting in continuous efforts despite difficulties to achieve future goals.


#### 2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

The video talks about how the importance of a growth mindset where people believe that skills and intelligence can be developed and they always focus on the process of getting better in something rather than just focusing on the outcomes or results.

#### 3. What is the Internal Locus of Control? What is the key point in the video?

The belief that we have power over our fate and lives is known as the internal locus of control. The primary message conveyed in the video was that those with an internal locus of control tend to be more motivated than those with an external locus of control, or those who feel powerless over their circumstances.

#### 4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).

The key points to build growth mindset are:

1. Believe in your ability to figure things out.
2. Question your assumptions.
3. Develop your own life curriculum.
4. Honor the struggle.
   
#### 5. What are your ideas to take action and build Growth Mindset?

My idea to build growth mindset is: Learn new ideas. Then, practice solving problems. When you solve them, you'll feel motivated because you know it's your effort that helped you. This shows that skills come from practice, not just natural talent, which builds a growth mindset.
