# Listening and Active Communication

#### 1. What are the steps/strategies to do Active Listening? (Minimum 6 points)

The steps to do Active Listening are: 
1. Avoid getting distracted by your thoughts. Instead, focus on the speaker and topic.
2. Do not interrupt the other person. Respond after they've finished.
3. Use phrases that show you're interested in the topic.
4. Use body language to show that you're listening.
5. Take notes during important conversations.
6. Paraphrase what others have said to make sure that you're on the same page.

#### 2. According to Fisher's model, what are the key points of Reflective Listening?

The key points of Reflective Listening are:

1. Listening more than talking so that the other person can fully express themselves.
2. Trying to understand the emotions behind someone's words and not just focusing on the facts or ideas.
3. Responding with acceptance and empathy.
4. Restating and clarifying what others have said to ensure accurate understanding.
5. Trying to understand the speaker's perspective without judgment.

#### 3. What are the obstacles in your listening process?

The obstacle in my listening process is that I am unable to make eye contact with the person I'm talking to.

#### 4. What can you do to improve your listening?

To enhance my listening, I will consciously work on making eye contact with the speaker.

#### 5. When do you switch to Passive communication style in your day to day life?

I switch to Passive communication in my day to day life when I don't communicate my dissatisfaction to the hair stylist when I'm unhappy with my hairstyle at the salon.

#### 6. When do you switch into Aggressive communication styles in your day to day life?

I switch to aggressive communication in my day-to-day life when I encounter someone intentionally trying to irritate or hurt others. For example, when an unknown person repeatedly calls even after being told that it is the wrong number.

#### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I tend to use Passive Aggressive Communication with people close to me, such as my brother when he plans a trip without me.


#### 8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? 

To make my communication assertive, few steps I will apply in my life:
1. Recognizing and naming my feelings. Telling the other person how I feel.
2. I will be aware of the body language and will make sure that I don't come across as aggressive.
3. I will start being more assertive before the situations that are problematic to me become a regular occurrence.



