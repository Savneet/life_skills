# Good Practices for Software Development

#### 1. Which point(s) were new to you?
The points that are new to me:
1. There are tools like TimeLimit, and Freedom that can help us limit time on social media.
2. There are apps like Boosted to improve productivity.


#### 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
I need to improve on avoiding distractions like social media and focus on deep work. 
To make progress, I use the Pomodoro method of 25 minutes of deep work with a 5-minute break. I have also scheduled time for using social media. Only at scheduled times do I use social media and I can focus on my work for the rest of the time.